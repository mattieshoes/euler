package main

import(
    "fmt"
    "time"
    )

func main() {
    start := time.Now()
    sum := 0
    for year := 1901; year < 2001; year++ {
        for month := 1; month <= 12; month++ {
            if time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC).Weekday() == time.Sunday {
                sum++
            }
        }
    }
    fmt.Printf("20th century months starting with Sunday: %v\n", sum)
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
