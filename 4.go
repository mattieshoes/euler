package main

import (
    "fmt"
    "time"
    c "euler/common"
)

func main() {
    start := time.Now()
    best := 0
    for i:= 999; i >= 100; i-- {
        for j := 999; j >= 100; j-- {
            product := i * j
            if product < best {
                break
            }
            if c.IsPalindrome(product) {
                best = product
            }
        }
    }
    fmt.Printf("Product: %d\nElapsed: %v\n", best, time.Since(start))
}
