package main

import (
    "fmt"
    "time"
    c "euler/common"
    )

func main() {
    start := time.Now()
    var solutions [1500001]int
    for m := int64(2); m < 1225; m++ {
        for n := int64(1); n < m; n++ {
            if (n+m)%2 == 1 && c.GCD(n, m) == 1 {
                a := m * m + n * n
                b := m * m - n * n
                c := 2 * m * n
                p := a + b + c

                for p <= 1500000 {
                    solutions[p]++
                    p += a + b + c
                }
            }
        }
    }
    sum := 0
    for i := 0; i <= 1500000; i++ {
        if solutions[i] == 1 {
            sum++
        }
    }
    fmt.Printf("Singular solutions: %d\n", sum)
    fmt.Printf("Elapsed: %s\n", time.Since(start))
}
