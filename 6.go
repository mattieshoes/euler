package main

import (
    "fmt"
    "time"
    "math/big"
    )

func main() {
    start := time.Now()

    sumSquares := big.NewInt(0)
    squaredSum := big.NewInt(0)

    for i := int64(1); i <= 100; i++ {
        val := big.NewInt(i)
        squaredSum.Add(squaredSum, val)
        val.Exp(val, big.NewInt(2), nil)
        sumSquares.Add(sumSquares, val)
    }
    squaredSum.Exp(squaredSum, big.NewInt(2), nil)
    squaredSum.Sub(squaredSum, sumSquares)
    fmt.Println(squaredSum)
    fmt.Printf("Elapsed: %s\n", time.Since(start))
}
