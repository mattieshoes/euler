package main

import (
    "fmt"
    "time"
    c "euler/common"
    )

func main() {
    start := time.Now()

    ch := make(chan uint64)
    go c.PrimeIterator(ch)
    for i, p := 1, <-ch; i <= 10001; i, p = i+1, <-ch {
        fmt.Println(i, p)
    }
    fmt.Printf("Elapsed: %s\n", time.Since(start))
}
