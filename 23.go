package main

import(
    "fmt"
    "time"
    c "euler/common"
    )

func main() {
    start := time.Now()

    // compile list of abundant numbers
    abundant := make([]uint64, 0)
    for i := uint64(12); i <= 28123; i++ {
        d := c.GetDivisors(i)
        sum := uint64(0)
        for _, v := range d { sum += v }
        if sum > i * 2 {
           abundant = append(abundant, i) 
        }
    }
   
    // generate all sums and remove them
    abundantsum := make([]bool, 28124)
    for i := 0; i < len(abundant); i++ {
        for j := i; j < len(abundant); j++ {
            v := abundant[i] + abundant[j]
            if v > 28123 { break}
            abundantsum[v] = true
        }
    }

    // step through and add up all the numbers left
    sum := uint64(0)
    for i, j := range(abundantsum) {
        if j == false {
            sum += uint64(i) 
            fmt.Println(i)
        }
    }

    fmt.Printf("Sum: %v\n", sum)
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
