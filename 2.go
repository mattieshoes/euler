package main

import (
    "fmt"
    "time"
    )


func main() {
    start := time.Now()

    fib := []int{1, 2}

    for fib[len(fib)-1] + fib[len(fib)-2] < 4e6 {
        fib = append(fib, fib[len(fib)-1] + fib[len(fib)-2])
    }
    sum := 0
    for _, val := range fib {
        if(val&1 == 0) {
            sum += val
        }
    }
    fmt.Printf("Sum: %d\nTime: %s\n", sum, time.Since(start))
}
