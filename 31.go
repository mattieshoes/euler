package main

import(
    "fmt"
    "time"
)

func search(left int, coinvalues []int) int {
    if(coinvalues[0] == 1) {
        return 1
    }
    sum := 0
    for i := 0; coinvalues[0] * i <= left; i++ {
        sum += search(left-i*coinvalues[0], coinvalues[1:])
    }
    return sum
}

func main() {
    start := time.Now()
    coinvalues := []int{200,100,50,20,10,5,2,1}
    fmt.Printf("Ways: %v\n", search(200, coinvalues))
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
