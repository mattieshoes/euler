package main

import(
    "fmt"
    "time"
    "math/big"
    )

func main() {
    start := time.Now()

    a := big.NewInt(0)
    b := big.NewInt(0)
    a.MulRange(int64(1),int64(40))
    b.MulRange(int64(1),int64(20))

    b.Mul(b, b)
    a.Div(a, b)

    fmt.Println(a)
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
