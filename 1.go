package main

import (
    "fmt"
    "time"
    )

func sum(from, to, step int, ch chan<- int) {
    sum := 0
    for i := from; i < to; i+=step {
        sum += i
    }
    ch <- sum
}

func main() {
    start := time.Now()

    add := make(chan int)
    sub := make(chan int)

    go sum(3, 1000, 3, add)
    go sum(5, 1000, 5, add)
    go sum(15, 1000, 15, sub)

    fmt.Printf("Sum: %d\nTime: %s\n", <-add + <-add - <-sub, time.Since(start))
}
