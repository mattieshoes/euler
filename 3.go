package main

import (
    "fmt"
    "time"
    "math"
    )

func largestPrimeFactor(n int64, ch chan<- int64) {
    root := int64(math.Sqrt(float64(n))+1)
    lpf := int64(0) 
    results := make(chan int64)
    numResults := 0

    for i := int64(2); i <= root; i++ {
        if n % i == 0 {
            go largestPrimeFactor(i, results)
            go largestPrimeFactor(n/i, results)
            numResults += 2
        }
    }
    for ; numResults > 0; numResults-- {
        val := <-results
        if(val > lpf) {
            lpf = val
        }
    }
    if lpf == 0 {
        ch <- n
    } else {
        ch <- lpf
    }
}


func main() {
    start := time.Now()
    ch := make(chan int64)
    go largestPrimeFactor(int64(600851475143), ch)
    fmt.Printf("Largest Prime Factor: %d\nElapsed: %s\n", <- ch, time.Since(start))
}
