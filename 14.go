package main

import(
    "fmt"
    "time"
    )



func collatz(v uint64, m map[uint64]uint64) uint64 {

    //check for solution
    val, ok := m[v]
    if ok {
        return val
    }
    
    //do collatz calculation and call collatz recurisvely
    if v % 2 == 0 {
        val = collatz(v/2, m)
    } else {
        val = collatz(v*3+1, m)
    }

    // write solution into map
    m[v] = val+1
    return val+1
}

func main() {
    start := time.Now()
    m := map[uint64]uint64{1:1}
    longest := uint64(1)
    for i := uint64(2); i < 1e6; i++ {
        result := collatz(i, m)
        if result > m[longest] { longest = i }
    }
    fmt.Printf("Longest: %v (%v steps)\n", longest, m[longest])
    fmt.Printf("Elapsed: %s\n", time.Since(start))
}
