package common

func TriangleIterator(results chan<- uint64) {
    sum := uint64(0)
    for cur := uint64(1); ; cur++ {
        sum += cur
        results <- sum
    }
}
