package common

import "strconv"

func SumDigits(s string) uint64 {
    sum := uint64(0)
    
    for i := 0; i < len(s); i++ {
        val, _ := strconv.ParseUint(s[i:i+1], 10, 64)
        sum += val
    }
    return sum
}
