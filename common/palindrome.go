package common

import "strconv"

func IsPalindrome(n interface{}) bool {
    var n_str string
    if _ , ok := n.(string); ok {
        n_str = n.(string)
    } else if n_int64, ok := n.(int64); ok {
        n_str = strconv.FormatInt(n_int64, 10)
    } else if n_int, ok := n.(int); ok {
        n_str = strconv.FormatInt(int64(n_int), 10)
    } else {
        return false
    }
    if n_str == ReverseString(n_str) { 
        return true 
    }
    return false
}
