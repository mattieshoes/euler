package common

import "math"

func GetDivisors(val uint64) []uint64 {
    stop := uint64(math.Sqrt(float64(val)))
    div := make([]uint64, 0)
    for i := uint64(1); i <= stop; i++ {
        if val % i == 0 {
            if(val / i == i) {
                div = append(div, i)
            } else {
                div = append(div, i, val/i)
            }
        }
    }
    return div
}
