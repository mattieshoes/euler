package common

func GCD(a, b int64) int64 {
    if b == 0 { return a }
    if b > a { 
        GCD(b, a) 
    } else {
        a = a % b
    }
    return GCD(b, a)
}
