package common

import "math"

func PrimeIterator(results chan<- uint64) {
    primes := []uint64{2}
    results <- primes[0]

    for testnum := uint64(3); ; testnum += 2 {
        stop := uint64(math.Sqrt(float64(testnum)) + 1)
        found := false  
        for divindex := 0; divindex < len(primes); divindex++ {
            if primes[divindex] > stop { break }
            if testnum % primes[divindex] == 0 {
                found = true
                break
            }
        }
        if found == false {
            primes = append(primes, testnum)
            results <- testnum
        }
    }
}
