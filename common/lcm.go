package common

func LCM(a, b int64) int64 {
    gcd := GCD(a, b)
    return a*b/gcd
}
