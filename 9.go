package main

import (
    "fmt"
    "time"
    )

func main() {
    start := time.Now()
    found := false
    for a := uint64(1); a < 1000; a++ {
        for b := a+1; b < 1000; b++ {
            if 1000 - a - b <= b { break }
            c := 1000 - a - b
            if a * a + b * b == c * c {
                found = true
                fmt.Printf("Product: %v\n", a*b*c)
            }
        }
        if found == true { break }
    }
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
