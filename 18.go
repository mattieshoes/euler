package main

import(
    "fmt"
    "time"
    )

func main() {
    start := time.Now()

    triangle := make([][]uint, 15)
    triangle[0]  = []uint{75}
    triangle[1]  = []uint{95, 64}
    triangle[2]  = []uint{17, 47, 82}
    triangle[3]  = []uint{18, 35, 87, 10}
    triangle[4]  = []uint{20,  4, 82, 47, 65}
    triangle[5]  = []uint{19,  1, 23, 75,  3, 34}
    triangle[6]  = []uint{88,  2, 77, 73,  7, 63, 67}
    triangle[7]  = []uint{99, 65,  4, 28,  6, 16, 70, 92}
    triangle[8]  = []uint{41, 41, 26, 56, 83, 40, 80, 70, 33}
    triangle[9]  = []uint{41, 48, 72, 33, 47, 32, 37, 16, 94, 29}
    triangle[10] = []uint{53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14}
    triangle[11] = []uint{70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57}
    triangle[12] = []uint{91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48}
    triangle[13] = []uint{63, 66,  4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31}
    triangle[14] = []uint{ 4, 62, 98, 27, 23,  9, 70, 98, 73, 93, 38, 53, 60,  4, 23}

    for r := 13; r >= 0; r-- {
        for c, val := range triangle[r] {
            if triangle[r+1][c+1] > triangle[r+1][c] {
                triangle[r][c] = triangle[r+1][c+1] + val
            } else {
                triangle[r][c] = triangle[r+1][c] + val
            }
        }
    }
    fmt.Printf("Max: %v\n", triangle[0][0])
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
