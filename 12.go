package main

import(
    "fmt"
    "time"
    c "euler/common"
    )

func main() {
    start := time.Now()
    ch := make(chan uint64, 2)
    go c.TriangleIterator(ch)

    for val := range(ch) {
        if len(c.GetDivisors(val)) > 500 {
            fmt.Println(val)
            break
        }
        
    }
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
