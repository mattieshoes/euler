package main

import(
    "fmt"
    "time"
    c "euler/common"
    )



func main() {
    start := time.Now()
    sum := uint64(0)
    for i := uint64(2); i < 10000; i++ {
        a := c.GetDivisors(i)
        asum := uint64(0)
        for _, v := range a { asum += v }
        asum -= i
        if asum > 1 && asum < 10000 && asum != i {
            b := c.GetDivisors(asum)
            bsum := uint64(0)
            for _, v := range b { bsum += v }
            bsum -= asum
            if bsum == i {
                sum += asum + bsum
            }
        }
    }
    sum /= 2
    fmt.Printf("Sum: %v\n", sum)
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
