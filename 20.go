package main

import(
    "fmt"
    "time"
    "math/big"
    c "euler/common"
    )

func main() {
    start := time.Now()
    var i big.Int
    i.MulRange(1, 100)
    sum := c.SumDigits(i.String())
    fmt.Printf("Sum: %v\n", sum)
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
