package main

import (
    "fmt"
    "time"
    c "euler/common"
)

func main() {
    start := time.Now()
    lcm := int64(11)
    for i := int64(12); i <= 20; i++ {
        lcm = c.LCM(lcm, i)
    }

    fmt.Printf("LCM of 1-20: %d\n", lcm)
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
