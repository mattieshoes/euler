package main

import (
    "fmt"
    "time"
    "math/big"
    c "euler/common"
    )

func main() {
    start := time.Now()

    a := big.NewInt(2)
    b := big.NewInt(1000)
    a.Exp(a, b, nil)
    sum := c.SumDigits(a.String())
    fmt.Printf("Sum: %v\n", sum)
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
