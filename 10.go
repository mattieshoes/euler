package main

import (
    "fmt"
    "time"
    c "euler/common"
    )

func main() {
    start := time.Now()
    ch := make(chan uint64, 2)
    go c.PrimeIterator(ch)

    sum := uint64(0)
    for n:= <-ch; n < 2e6; n = <-ch {
        sum += n
    }
    fmt.Printf("Sum: %v\n", sum)
    fmt.Printf("Elapsed: %v\n", time.Since(start))
}
